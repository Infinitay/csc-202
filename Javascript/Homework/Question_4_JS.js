function writeOut(i) {
    if (i < 0)
        throw RangeError; //throw "Only positive real numbers."
    if (i !== 0)
        return "(1 / " + i + ") + "+writeOut(i-1);
    else // base case
        return "";
}

function sum(i) {
    if (i < 0)
        throw RangeError; //throw "Only positive real numbers."
    if (i === 1) // base case
        return 1;
    else
        return  1 / i + sum(i - 1);
}

try {
    for (var i = 1; i <= 10; i++) {
        console.log("(1 / " + i + "): " + (writeOut(i)) + "| Sum: " + sum(i));
    }
} catch (e) {
    console.log("Exercise only states to print from i = 1 to i = 10, nice try.");
    console.log("Please only enter positive real numbers next time.");
}

// https://jsbin.com
// codepad.remoteinterview.io