package Homework.Chapter_28;

import Homework.Chapter_28.util.AbstractGraph;
import Homework.Chapter_28.util.Graph;
import Homework.Chapter_28.util.UnweightedGraph;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by REDACTED on 11/29/2017.
 */
public class Question23_Controller {

    @FXML
    private AnchorPane pane;

    @FXML
    void initialize() {
        pane.setOnMouseClicked(e -> {
            if (!isInsideARectangle(new Point2D(e.getX()-20, e.getY()-20))) {
                // Add a new circle
                pane.getChildren().add(new Rectangle(e.getX()-20, e.getY()-20, 40, 40));
                colorIfConnected();
            }
        });
    }

    /** Returns true if the point is inside an existing circle */
    private boolean isInsideARectangle(Point2D p) {
        for (Node rect: pane.getChildren())
            if (rect.contains(p))
                return true;

        return false;
    }

    /** Color all circles if they are connected */
    private void colorIfConnected() {
        if (pane.getChildren().size() == 0)
            return; // No rect in the pane

        // Build the edges
        List<AbstractGraph.Edge> edges = new ArrayList<>();
        for (int i = 0; i < pane.getChildren().size(); i++)
            for (int j = i + 1; j < pane.getChildren().size(); j++)
                if (overlaps((Rectangle) (pane.getChildren().get(i)), (Rectangle) (pane.getChildren().get(j)))) {
                    edges.add(new AbstractGraph.Edge(i, j));
                    edges.add(new AbstractGraph.Edge(j, i));
                }

        // Create a graph with circles as vertices
        Graph<Node> graph = new UnweightedGraph<>(pane.getChildren(), edges);
        AbstractGraph<Node>.Tree tree = graph.dfs(0); // a DFS tree
        boolean isAllRectConnected = pane.getChildren().size() == tree.getNumberOfVerticesFound();

        for (Node rect: pane.getChildren()) {
            if (isAllRectConnected) { // All circles are connected
                ((Rectangle)rect).setFill(Color.RED);
            }
            else {
                ((Rectangle)rect).setStroke(Color.BLACK);
                ((Rectangle)rect).setFill(Color.WHITE);
            }
        }
    }

    public boolean overlaps(Rectangle rect1, Rectangle rect2) {
        //pane.getChildren().add(new Circle(rect1.getWidth()/2, rect1.getHeight()/2, 1));
        return new Point2D((rect1.getX()+20), rect1.getY()+20).
                distance(rect2.getX()+20, rect2.getY()+20)
                <= rect1.getWidth()/2 + rect2.getWidth()/2;
    }
}
