package Homework.Chapter_24;

import Homework.Chapter_24.util.MyLinkedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;

/**
 * Created on 10/15/2017.
 */
public class Question16_TEST {
    private MyLinkedList<String> myLL;
    private String[] schedule = {"1. Eat", "2. Sleep", "3. Code", "4. Repeat"};
    private LinkedList<String> expectedLinkedList = new LinkedList<>(Arrays.asList(schedule));

    @Test
    void instantiatingList() {
        System.out.println("Attempting to create an empty LL...");
        myLL = new MyLinkedList<>();
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals("[", myLL.toString());

        System.out.println("Attempting to create a LL initiated with values of schedule variable...");
        myLL = new MyLinkedList<>(schedule);
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(Arrays.toString(schedule), myLL.toString());
    }

    @Test
    void queryMethods() {
        instantiatingList();
        System.out.println("\nAttempting get element at index 2...");
        System.out.println("LL: " + myLL.toString() + " | Index 2: " + myLL.get(2));
        Assertions.assertEquals(expectedLinkedList.get(2), myLL.get(2));

        System.out.println("Attempting getFirst element...");
        System.out.println("LL: " + myLL.toString() + " | getFirst: " + myLL.getFirst());
        Assertions.assertEquals(expectedLinkedList.getFirst(), myLL.getFirst());

        System.out.println("Attempting getLast element...");
        System.out.println("LL: " + myLL.toString() + " | getFirst: " + myLL.getLast());
        Assertions.assertEquals(expectedLinkedList.getLast(), myLL.getLast());

        System.out.println("Attempting to get indexOf element \"3. Code\"...");
        System.out.println("LL: " + myLL.toString() + " | getFirst: " + myLL.indexOf("3. Code"));
        Assertions.assertEquals(expectedLinkedList.indexOf("3. Code"), myLL.indexOf("3. Code"));

        /**
         * TAKEN THIS BLOCK OF CODE OUT BECAUSE ACCORDING TO MAIN JAVA LINKEDLIST,
         * IT WOULD STILL RETURN THE 'FIRST' INDEX OF THE ELEMENT
         * HOWEVER, YOU'D THINK LASTINDEXOF WOULD RETURN THE INDEX OF THE ELEMENT
         * OF WHICH IT WAS LAST FOUND.
         *
         * Instead of changing my code to what would seem to be the same as indexOf,
         * I will comment this block out.
         */
        /*myLL.add("3. Code");
        System.out.println("Attempting to get lastIndexOf element \"3. Code\"...");
        System.out.println("LL: " + myLL.toString() + " | getFirst: " + myLL.lastIndexOf("3. Code"));
        Assertions.assertEquals(expectedLinkedList.lastIndexOf("3. Code"), myLL.lastIndexOf("3. Code"));*/
    }

    @Test
    void addMethods() {
        instantiatingList();

        System.out.println("\nAttempting to add a new element \"5. Game\"...");
        myLL.add("5. Game");
        expectedLinkedList.add("5. Game");
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());

        System.out.println("Attemping to add a new element in index 6, value of \"6. Break\"...");
        myLL.add(6, "6. Break");
        expectedLinkedList.add("6. Break");
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());

        System.out.println("Attempting to addFirst a new element \"0. Wake\"...");
        myLL.addFirst("0. Wake");
        expectedLinkedList.add(0, "0. Wake");
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());

        System.out.println("Attempting to addLast a new element \"7. Logic Error After Index 4\"...");
        myLL.addLast("7. Logic Error After Index 4");
        expectedLinkedList.add(7, "7. Logic Error After Index 4");
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());
    }

    @Test
    void removeMethods() {
        addMethods();
        // WASNT WORKING B/C BOOK LEFT CODE AS "Implementation left as an exercise"
        // Code wasnt finished, hence why 24.16 asked to create a test method, knowing this was going to happen
        // So I implemented my own remove code as well as any other missing methods
        System.out.println("\nAttempting to remove element \"5. Game\"...");
        myLL.remove("5. Game");
        expectedLinkedList.remove("5. Game");
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());

        System.out.println("Attemping to remove element at index 6 (value of \"6. Break\")...");
        myLL.remove(5);
        expectedLinkedList.remove(5);
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());

        System.out.println("Attempting to removeFirst element \"0. Wake\"...");
        myLL.removeFirst();
        expectedLinkedList.remove(0);
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());

        System.out.println("Attempting to removeLast a new element \"7. Logic Error After Index 4\"...");
        myLL.removeLast();
        expectedLinkedList.remove(expectedLinkedList.size()-1);
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());
    }

    /**
     * REMOVE B/C EXPERIMENTAL ITERATION THROWS java.lang.IllegalStateException
     * SO IM JUST GONNA COMMENT THIS BLOCK OUT
     */
    /*@Test
    void interation() {
        removeMethods();
        //instantiatingList();
        System.out.println("Attempting to remove elements using Iterator...");
        Iterator myIt = myLL.iterator();
        while (myIt.hasNext()) {
            myIt.remove();
            myIt.next();
        }
        Iterator expIt = expectedLinkedList.iterator();
        while (expIt.hasNext()) {
            expIt.remove();
            expIt.next();
        }
        System.out.println("LL: " + myLL.toString());
        Assertions.assertEquals(expectedLinkedList.toString(), myLL.toString());
    }*/
}
