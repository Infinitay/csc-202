package Homework.Chapter_21;

//import Assignments.LoginApplication.UserHandler.Gender;
/**
 * Created on 9/20/2017.
 */
public class Question11_Controller {

    /*@FXML
    private ComboBox<Integer> rankYearCB;

    @FXML
    private ComboBox<Gender> genderCB;

    @FXML
    private TextField nameField;

    @FXML
    private Button findRankBtn;

    @FXML
    private Label rankLabel;

    private HashMap<String, Integer> rankList;

    @FXML
    void initialize() {
        Platform.runLater(() -> {
            List<Integer> yearList = new ArrayList<>(20);
        *//*File[] files = new File("Homework/Chapter_21/Resources").listFiles();
        if (files != null)
        for (File file : files) {
            yearList.add(Integer.parseInt(file.getName().substring(3, 7)));
            System.out.print(file.getName().substring(3, 7) + ", ");
        }
        System.out.println();*//*
            for (int year = 1990; year < 2017; year++)
                yearList.add(year);

            rankYearCB.getItems().setAll(yearList);
            genderCB.getItems().setAll(Gender.values());
            rankList = new HashMap();
        });
    }

    *//*
     TODO
      - implement "caching" system, where if the year AND GENDER (consider if a 'unisex' name)
      -- doesn't change, then don't generate a new hashmap
     *//*
    @FXML
    void findRank(ActionEvent event) {
        if (rankYearCB.getValue() != null && genderCB.getValue() != null && !nameField.getText().isEmpty()) {
            String name = nameField.getText().trim();
            int year = rankYearCB.getValue();
            Gender gender = genderCB.getValue();
            //System.out.println(genderCB.getValue().toString().substring(0, 1));
            rankList = generateHashMap(year, gender.toString().substring(0, 1));
            if (rankList.containsKey(name.toLowerCase())) //don't even need to check for empty/null, cause this will do it for us (technically ofc)!
                    rankLabel.setText(name + " (" + gender + ") was ranked at #" + rankList.get(name.toLowerCase()) + " in " + year + ".");
            else
                    rankLabel.setText("Couldn't find name in database.");
        } else {
            rankLabel.setText("Please properly input all the fields.");
        }
    }

    private HashMap generateHashMap(int year, String gender) {
        String url = "https://gitlab.com/Infinitay/Resources/raw/master/Homework/Chapter_21/yob";
        URL namesURL;
        HashMap<String, Integer> tempRankList = new HashMap(1000);
        try {
            namesURL = new URL(url.concat(String.valueOf(year)).concat(".txt"));
            Scanner scanner = new Scanner(namesURL.openStream());
            while (scanner.hasNext()) {
                List<String> tempData = new ArrayList<>();
                tempData.addAll(Arrays.asList(scanner.nextLine().split(",")));
                //System.out.println("Temp data: " + tempData);
                if (tempData.contains(gender)) {
                    tempRankList.put(tempData.get(0).toLowerCase(), Integer.valueOf(tempData.get(2)));
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            showError("Couldn't load URL.");
        } catch (IOException e) {
            e.printStackTrace();
            showError("Couldn't read data from URL.");
        }
        //System.out.println(rankList.size());
        return tempRankList;
    }

    private void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.showAndWait();
    }*/
}
