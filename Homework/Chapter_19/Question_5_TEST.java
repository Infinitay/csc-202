package Homework.Chapter_19;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

/**
 * Created on 8/27/2017.
 */
class Question_5_TEST {

    @Test
    void max() {
        Integer[] randX = new Integer[25];
        for (int i = 0; i < randX.length; i++)
            randX[i] = new Random().nextInt();

        for (int i = 0; i < randX.length; i++) {
            Assertions.assertEquals(expectedMax(randX), Question_5.max(randX), "i = " + i);
        }
    }

    private <E extends Comparable<E>> E expectedMax(E[] list) {
        //Collections.max(Arrays.asList(list));
        Arrays.sort(list); //sorts from desc to asc | can do #sort(list, Collections.reverseOrder()) and just return list[0]
        return list[list.length-1];
    }

}