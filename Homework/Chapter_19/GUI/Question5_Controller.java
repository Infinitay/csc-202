package Homework.Chapter_19.GUI;

import Homework.Chapter_19.Question_5;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class Question5_Controller implements Initializable {

    @FXML
    private Label maxLabel;

    @FXML
    private Button answerButton;

    @FXML
    private TextField listTextField;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        /*listTextField.focusedProperty().addListener((observable, oldValue, newValue) -> { //https://stackoverflow.com/questions/30935279/javafx-input-validation-textfield
            if (!newValue) { //when focus lost
                if(!listTextField.getText().matches("[1-5]\\.[0-9]|6\\.0")){
                    //when it not matches the pattern (1.0 - 6.0)
                    //set the textField empty
                    listTextField.setText("x");
                }
            }

        });*/
    }

    @FXML
    void compute(ActionEvent event) {
        try { //https://stackoverflow.com/questions/15615890/recommended-way-to-restrict-input-in-javafx-textfield
            String[] unparsed = listTextField.getText().split(", ");
            Integer[] array = new Integer[unparsed.length];
            for (int i = 0; i < unparsed.length; i++) {
                array[i] = Integer.parseInt(unparsed[i]);
            }
            maxLabel.setText(String.valueOf(Question_5.max(array)));
        } catch(Exception e) { //java.lang.NumberFormatException: For input string: "d"
            new Alert(Alert.AlertType.ERROR, e.toString() + "\n Please make sure to input numbers seperated by \" \' \""
            + "\n Exercise states i = 10.").show();
            listTextField.setText("");
            int[] rand = new int[10];
            for (int i = 0; i < rand.length; i++)
                rand[i] = new Random().nextInt(100);
            for (int x : rand)
                listTextField.setText(listTextField.getText() + x + ", ");
            listTextField.setText(listTextField.getText(0, listTextField.getText().length()-2)); //removes extra ", " from end
        }
    }
}
