package Homework.Chapter_19;

import java.util.Random;

/**
 * Created on 8/29/2017.
 */
public class Question_5 {
    public static void main(String[] args) {
        //DO REFERENCES, NOT PRIMITIVE, so Integer NOT int!!!
        //Integer[] x  = {0, 1, 2, 3, 4, 5}; //Integer for int

        Integer[] randX = new Integer[25];
        for (int i = 0; i < randX.length; i++)
            randX[i] = new Random().nextInt();

        //Double[] d = {1.1, 1.2, 01.4, 1.43};
        //String[] s = {"Hello", "Aaaaa", "Zzzzz"}; //String for string
        //Character[] c = {'A', 'z', 'a'}; //Character for char
        System.out.println("Max element in array \'" + randX.getClass().getSimpleName() + "\': " + max(randX));
    }

    // #compareTo -1 if <, 0 if =, 1 if >
    public static <E extends Comparable<E>> E max(E[] list) {
        E temp = list[0];
        for (int i = 1; i < list.length; i++) {
            if (list[i].compareTo(temp) == 1)
                temp = list[i];
            System.out.println("i: " + list[i] + " | Temp: " + temp + " | Byte val: " + list[i].hashCode()); // for debugging
        }
        return temp;
    }

    // This method is experimental. It's for also finding the max for other data types than integers.
    // However the one problem with this is that for chars, A < a which seems wrong seeing as it's a capital
    // letter. Therefore commented this code out, but in theory, it works for Strings and chars (not accounting for upp/low case)
    /*
    public static <E extends Comparable<E>> E max(E[] list) {
        E temp = list[0];
        for (int i = 0; i < list.length; i++) {
            if (list[i].hashCode() > temp.hashCode())
                temp = list[i];
            System.out.println("i: " + list[i] + " | Temp: " + temp + " | Byte val: " + list[i].hashCode());
        }
        return temp;
    }*/
}
