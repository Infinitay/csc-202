package Homework.Chapter_29;

import Homework.Chapter_29.util.WeightedEdge;
import Homework.Chapter_29.util.WeightedGraph;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 12/10/2017.
 */
public class Controller {

    @FXML
    private AnchorPane mainPane, graphPane;

    @FXML
    private TextField vNameField, xCordField, yCordField;

    @FXML
    private Button addVertexButton;

    @FXML
    private TextField fromVertexField, toVertexField, weightField;

    @FXML
    private Button addEdgeButton;

    @FXML
    private TextField pathFromField, pathToField;

    @FXML
    private Button findPathButton, startOverButton;

    final int X_OFFSET = 25;
    final int Y_OFFSET = 20;
    final int X_BOUNDS = 750;
    final int Y_BOUNDS = 400;

    private List<Vertex> vertices;
    private List<WeightedEdge> edges;
    private WeightedGraph<Vertex> weightedGraph;

    @FXML
    void initialize() {
        vertices = new ArrayList<>();
        edges = new ArrayList<>();
        weightedGraph = new WeightedGraph<>(vertices, edges);
    }

    @FXML
    void addVertex(ActionEvent event) { //currently doesnt stop duplicates
        int x = 0, y = 0;

        if (Integer.parseInt(xCordField.getText().trim()) != X_BOUNDS)
            x = Integer.parseInt(xCordField.getText().trim()) + X_OFFSET;
        else
            x = X_BOUNDS;
        //------------------------------------\\
        if (Integer.parseInt(yCordField.getText().trim()) != Y_BOUNDS)
            y = Integer.parseInt(yCordField.getText().trim()) + Y_OFFSET;
        else
            y = Y_BOUNDS;

        if (x > X_BOUNDS || y > Y_BOUNDS)
            new Alert(Alert.AlertType.ERROR, "Out of bounds.").show();
        else {
            graphPane.getChildren().add(new Circle(x, y, 8)); // Display a vertex
            graphPane.getChildren().add(new Text(x - 4, y - 9, vNameField.getText().trim()));
            /**
             * If you want to display text content not associated with input, you use Text
             * A Text is a geometric shape (like a Rectangle or a Circle),
             * while Label is a UI control (like a Button or a CheckBox).
             */
            vertices.add(new Vertex(vNameField.getText().trim(), x, y));
        }
        vNameField.setText("");
        xCordField.setText("");
        yCordField.setText("");
    }

    @FXML
    void addEdge(ActionEvent event) {
        int u = 0, v = 0;

        u = Integer.parseInt(fromVertexField.getText().trim());
        v = Integer.parseInt(toVertexField.getText().trim());

        Vertex from = null, to = null;
        for (Vertex x : vertices) {
            if (x.getName().equals(String.valueOf(u))) {
                from = x;
            } else if (x.getName().equals(String.valueOf(v))) {
                to = x;
            }
        }

        if (from != null && to != null) { //currently doesnt stop duplicates
            graphPane.getChildren().add(new Line(from.getX(), from.getY(), to.getX(), to.getY()));
            graphPane.getChildren().add(new Text(((from.getX() + to.getX()) / 2 )+ 4, ((from.getY() + to.getY()) / 2) + 9, weightField.getText().trim()));
            edges.add(new WeightedEdge(u, v, Integer.parseInt(weightField.getText().trim())));
        } else
            new Alert(Alert.AlertType.ERROR, "Please double check to see if the from and to vertices even exist...").show();

        fromVertexField.setText("");
        toVertexField.setText("");
        weightField.setText("");
    }

    @FXML
    void findPath(ActionEvent event) {
       // weightedGraph
    }

    @FXML
    void startOver(ActionEvent event) {

    }
}
