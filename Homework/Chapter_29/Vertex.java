package Homework.Chapter_29;

/**
 * Created on 12/10/2017.
 */
public class Vertex {
    /*final int X_OFFSET = 25;
    final int Y_OFFSET = 20;
    final int X_BOUNDS = 750;
    final int Y_BOUNDS = 400;*/

    private int x, y;
    private String name;

    Vertex(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
        // already handled
/*        if (this.x!= X_BOUNDS)
            this.x = this.x + X_OFFSET;
        else
            this.x = X_BOUNDS;
        //------------------------------------\\
        if (this.y != Y_BOUNDS)
            this.y = this.y + Y_OFFSET;
        else
            this.y = Y_BOUNDS;*/
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "x=" + x +
                ", y=" + y +
                ", name='" + name + '\'' +
                '}';
    }
}

