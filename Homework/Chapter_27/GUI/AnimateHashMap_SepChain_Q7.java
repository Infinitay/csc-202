package Homework.Chapter_27.GUI;

import Homework.Chapter_27.util.MyHashMap;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created on 10/8/2017.
 */
public class AnimateHashMap_SepChain_Q7 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("Question7_GUI.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setTitle("HashMap Animation (Seperate Chaining)");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        MyHashMap<Integer, Integer> myHashMap = new MyHashMap<Integer, Integer>(11);
        myHashMap = new MyHashMap<Integer, Integer>(8, true);
        System.out.println(Arrays.toString(myHashMap.getHashTable()));
        for (int i = 0; i < 1000; i++) {
            myHashMap.put(i, i);
            System.out.println("x");
        }
        System.out.println(Arrays.toString(myHashMap.getHashTable()));
        System.out.println(myHashMap);
    }
}
