package Homework.Chapter_27.GUI;

import Homework.Chapter_27.util.MyHashMap;
import javafx.animation.FadeTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;

/**
 * Created on 10/8/2017.
 */
public class Question7_Controller {

    @FXML
    private HBox stackHolder;

    @FXML
    private VBox stackVBox;

    @FXML
    private StackPane stackPane;

    @FXML
    private Circle circle;

    @FXML
    private Label label, dynamicLabel;

    @FXML
    private VBox stackVBox1, stackVBox2, stackVBox3, stackVBox4, stackVBox5, stackVBox6, stackVBox7, stackVBox8;

    @FXML
    private Slider speedSlider;

    @FXML
    private TextField inputBtn;

    @FXML
    private Button addBtn;

    @FXML
    private Button playBtn;

    @FXML
    private Button pauseBtn;

    @FXML
    private Button stepBtn;

    @FXML
    private Button resetBtn;

    private final double HEIGHT_FACTOR = 2.82; //(280/99)
    private int[] array;
    private SequentialTransition timeline;

    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            /*System.out.println(Arrays.toString(myHashMap.getHashTable()));
            for (int i = 0; i < 16; i++)
                myHashMap.put(i, i+1);
            System.out.println(Arrays.toString(myHashMap.getHashTable()));
            System.out.println(myHashMap.size());*/
            stackHolder.managedProperty().setValue(false);
            timeline = new SequentialTransition();
            timeline.setCycleCount(1);
            timeline.rateProperty().bindBidirectional(speedSlider.valueProperty());
        });

    }

    @FXML
    void add() {
        try {
            int temp = Integer.parseInt(inputBtn.getText());
                if (temp > -1 && temp < 100)
                    if (!stackPaneHashMap.containsKey(temp))
                        addEntryToHashMap(temp);
                    else
                        dynamicLabel.setText("Duplicate entry.");
                else
                    throw new IllegalArgumentException();
        } catch (Exception e) {
            new Alert(Alert.AlertType.ERROR, "Follow instructions please, why must you make everything so difficult >.> its 6 AM and im tryna go to bed already").show();
        }

    }

    @FXML
    void play() {
        //Platform.runLater(() -> {
            System.out.println("Sorting array and creating animation.");
            System.out.println("Done! Now attempting to play back.");
/*            System.out.println(timeline.getTotalDuration());
            System.out.println(timeline.getRate());*/
            timeline.play();
            timeline.setOnFinished(event -> resetTimeline());
        //});
    }

    private int skipTo = 0;
    @FXML
    void step() {
        Platform.runLater(() -> {
            speedSlider.valueProperty().setValue(0);
            timeline.rateProperty().bindBidirectional(speedSlider.valueProperty());
            timeline.play();
            skipTo = skipTo + 20;
            //sort(array);
            timeline.jumpTo(Duration.millis(skipTo));
            System.out.println(timeline.getCurrentTime());
        });

    }

    @FXML
    void reset() {
        stackPaneHashMap = new MyHashMap<>(8, true);
        resetTimeline();
        skipTo = 0;
        changeScene();
    }

    @FXML
    void pause() {
        timeline.pause();
    }

    @FXML
    void displayInfo(MouseEvent event) {
        new Alert(Alert.AlertType.INFORMATION, ((event.getSource()).toString()
                + "\n\n" + ((StackPane)event.getSource()).getLayoutBounds())
                + "\n\n" + ((StackPane)event.getSource()).getBoundsInParent()).show();
    }

    private void resetTimeline() {
        timeline.getChildren().clear();
        timeline = new SequentialTransition();
        timeline.setCycleCount(1);
        timeline.rateProperty().bindBidirectional(speedSlider.valueProperty());
    }

    private MyHashMap<Integer, StackPane> stackPaneHashMap = new MyHashMap<>();

    private void addEntryToHashMap(int data) {
        dynamicLabel.setText(data + ": hash = " + stackPaneHashMap.hash(data));
        timeline.getChildren().add(new PauseTransition(Duration.millis(250)));

        createStackPane(data);
        addToThisVBox(data).getChildren().add(stackPaneHashMap.get(data));

        FadeTransition ft = new FadeTransition(Duration.millis(1000), stackPaneHashMap.get(data));
        ft.setFromValue(0.0);
        ft.setToValue(1.0);
        timeline.getChildren().add(ft);
    }

    private void createStackPane(int data) {
        StackPane temp = new StackPane(cloneCircle(circle), cloneLabel(label, String.valueOf(data)));
        temp.setId(String.valueOf(data));
        stackPaneHashMap.put(data, temp);
        //return temp;
    }

    private VBox addToThisVBox(int data) {
        VBox returnThis = stackVBox;
        switch (stackPaneHashMap.hash(data)) {
            case 0:
                returnThis = stackVBox;
                break;
            case 1:
                returnThis = stackVBox1;
                break;
            case 2:
                returnThis = stackVBox2;
                break;
            case 3:
                returnThis = stackVBox3;
                break;
            case 4:
                returnThis = stackVBox4;
                break;
            case 5:
                returnThis = stackVBox5;
                break;
            case 6:
                returnThis = stackVBox6;
                break;
            case 7:
                returnThis = stackVBox7;
                break;
            case 8:
                returnThis = stackVBox8;
                break;
            default:
                new Alert(Alert.AlertType.ERROR, "beep boop, you found an error ughh").show();
                break;
        }

        return returnThis;
    }

    private void changeScene() {
        try {
            //To help pass parameters between scene
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Question7_GUI.fxml"));
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            Stage primaryStage = (Stage) stackHolder.getScene().getWindow(); //THIS IS IMPORTANT
            /*
             * The code above allows us to get the current (primary) stage
             * So we can switch scenes with ease, rather than creating a new stage constantly.
             */
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private Circle cloneCircle(Circle cloneThis) {
        Circle temp = new Circle(cloneThis.getRadius(), cloneThis.getFill());
        temp.smoothProperty().setValue(cloneThis.smoothProperty().getValue());
        temp.strokeProperty().setValue(cloneThis.strokeProperty().getValue());
        temp.strokeWidthProperty().setValue(cloneThis.strokeWidthProperty().getValue());
        temp.strokeDashOffsetProperty().setValue(cloneThis.strokeDashOffsetProperty().getValue());
        temp.strokeTypeProperty().setValue(cloneThis.strokeTypeProperty().getValue());
        temp.strokeLineCapProperty().setValue(cloneThis.strokeLineCapProperty().getValue());
        temp.strokeLineJoinProperty().setValue(cloneThis.strokeLineJoinProperty().getValue());
        temp.strokeMiterLimitProperty().setValue(cloneThis.strokeMiterLimitProperty().getValue());
        temp.styleProperty().setValue(cloneThis.styleProperty().getValue());
        //temp.getCssMetaData().addAll(cloneThis.getCssMetaData());
        //temp.applyCss();
        return temp;
    }

    private Label cloneLabel(Label cloneThis, String newData) {
        Label temp = new Label(newData);
        temp.setFont(cloneThis.getFont());
        temp.setTextFill(cloneThis.getTextFill());
        temp.alignmentProperty().setValue(cloneThis.alignmentProperty().getValue());
        //temp.setStyle(cloneThis.getStyle());
        //temp.getCssMetaData().addAll(cloneThis.getCssMetaData());
        //temp.applyCss();
        return temp;
    }
}
