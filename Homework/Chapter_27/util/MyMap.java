package Homework.Chapter_27.util;

import java.util.Set;

/**
 * Created by REDACTED on 11/6/2017.
 */
public interface MyMap<K, V> {
    /**
     * Remove all of the entries from this map
     */
    void clear();

    /**
     * Return true if the specified key is in the map
     */
    boolean containsKey(K key);

    /**
     * Returrns true if this map contains the specified value
     */
    boolean containsValue(V value);

    /**
     * Return a set of entries in the map
     */

    Set<MyEntry<K, V>> entrySet();

    /**
     * Return the value that matches the specified key
     */
    V get(K key);

    /**
     * Return true if this map doesn't contain any entries
     */
    boolean isEmpty();

    /**
     * Return a set consisting of the keys in this map
     */
    Set<K> keySet();

    /**
     * Add an entry (key, value) into the map
     * Return value of entry added
     */
    V put(K key, V value);

    /**
     * Remove an entry for the specified key
     */
    void remove(K key);

    /**
     * Return the number of mappings in this map
     */
    int size();

    /**
     * Return a set consisting of the values in this map
     */
    Set<V> values();

    /**
     * Define an inner class for MyEntry
     */
    public static class MyEntry<K, V> {
        K key;
        V value;

        public MyEntry(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }

        @Override
        public String toString() {
            return "[" + key + ", " + value + "]";
        }
    }
}
