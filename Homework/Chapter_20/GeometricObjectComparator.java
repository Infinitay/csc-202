package Homework.Chapter_20;

import java.util.Comparator;

/**
 * Created on 9/15/2017.
 */
public class GeometricObjectComparator implements Comparator<GeometricObject> { //Type GeometricObject, otherwise we cant override

    @Override
    public int compare(GeometricObject o1, GeometricObject o2) {
        // -1 if < || 0 if = || 1 if >
        double area1 = o1.getArea();
        double area2 = o2.getArea();
        if (area1 < area2)
            return -1; //If Object 1's area is less than area two's, return -1
        else if (area1 == area2)
            return 0; //If Object 1's area is equal to area two's, return 0
        else
            return 1; //If Object 1's area is greater than area two's, return 1
    }
}
