package Homework.Chapter_20;

/**
 * Created on 9/15/2017.
 */
public class Circle extends GeometricObject {

    private double radius;

    public Circle() {
        this.radius = 2;
    }

    public Circle(double r) {
        this.radius = r;
    }

    @Override
    public double getArea() {
        return Math.PI * (Math.pow(this.radius, 2));
    }

    @Override
    public double getPerimeter() {
        return 2 * (Math.PI * this.radius);
    }

    @Override
    public String toString() {
        return "Circle(" + this.radius + ")";
    }
}
