package Homework.Chapter_20;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Random;

/**
 * Created on 9/16/2017.
 */
class Question_21_Test {

    @Test
    void selectionSort() {
        GeometricObject[] array = new GeometricObject[10];
        for (int i = 0; i < 5; i++)
            array[i] = new Circle(((double) new Random().nextInt(10)));
        for (int i = array.length/2; i < array.length; i++)
            array[i] = new Rectangle((double) new Random().nextInt(10), (double) new Random().nextInt(10));

        GeometricObject[] expArray = array.clone();
        GeometricObject[] actArray = array.clone();
        System.out.println("Before sort: \nActual: " + Question_21.arrayToString(actArray) + "\nExpected: " + Question_21.arrayToString(expArray));
        Arrays.sort(expArray, new GeometricObjectComparator());
        Question_21.selectionSort(actArray, new GeometricObjectComparator());
        System.out.println("After sort: \nActual: " + Question_21.arrayToString(actArray) + "\nExpected: " + Question_21.arrayToString(expArray));

        for (int i = 0; i < array.length; i++)
        Assertions.assertEquals(expArray[i].toString(), actArray[i].toString());
        /*
         * Without toString(), it was returning false b/c it was comparing the ACTUAL objects against each other
         * and it was throwing false BECAUSE they had different ADDRESSES (test for yourself, remove the toString)
         * So instead, I will just compare the object names, still the same result, still works (AFAIK ofc)
         */
    }


}