package Homework.Chapter_20.GUI;

import Homework.Chapter_20.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created on 9/16/2017.
 */
public class Question21_Controller {

    @FXML
    private Button startOverBtn;

    @FXML
    private Button sortBtn;

    @FXML
    private Button genRandomBtn;

    @FXML
    private TextField circRad1;

    @FXML
    private TextField circRad2;

    @FXML
    private TextField circRad3;

    @FXML
    private TextField circRad4;

    @FXML
    private TextField circRad5;

    @FXML
    private TextField rectLen1;

    @FXML
    private TextField rectWid1;

    @FXML
    private TextField rectLen2;

    @FXML
    private TextField rectWid2;

    @FXML
    private TextField rectLen3;

    @FXML
    private TextField rectWid3;

    @FXML
    private TextField rectLen4;

    @FXML
    private TextField rectWid4;

    @FXML
    private TextField rectLen5;

    @FXML
    private TextField rectWid5;

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private VBox test;

    private List<TextField> fields = new ArrayList<>(10);

    @FXML
    void clearTables(ActionEvent event) {
        for (TextField x : fields)
            x.setText("");
        alertLimit = 0;
    }

    @FXML
    void showSortedList(ActionEvent event) {
        GeometricObject[] shapeList = new GeometricObject[10];

        if (areFieldsValid(getFields())) {
            for (int i = 0, j = getFields().size(); i < 10; i++, j--) {
                if (i < 5)
                    shapeList[i] = new Circle(Double.parseDouble(getFields().get(i).getText()));
                else
                    shapeList[i] = new Rectangle(Double.parseDouble(getFields().get(i).getText()), Double.parseDouble(getFields().get(j).getText()));
            }
            Question_21.selectionSort(shapeList, new GeometricObjectComparator());
            showMessage(Question_21.arrayToString(shapeList), "Sorted list");
        }
        alertLimit = 0;
    }

    @FXML
    void generateRandomValues(ActionEvent event) {
            for (int i = 0, j = getFields().size(); i < 15; i++, j--) {
                if (i < 5)
                    getFields().get(i).setText(String.valueOf(new Random().nextInt(100)));
                else {
                    getFields().get(i).setText(String.valueOf(new Random().nextInt(100)));
                    getFields().get(j).setText(String.valueOf(new Random().nextInt(100)));
                }
            }
        alertLimit = 0;
    }

    @FXML
    void initialize() {
        Platform.runLater(() -> {
            alertLimit = 0;
            fields = getFields();
        });
    }

    private void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.showAndWait();
    }

    private void showMessage(String message, String title) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message, ButtonType.OK);
        alert.setHeaderText(title);
        alert.showAndWait();
    }

    private int alertLimit = 0;

    private boolean isFieldValid(TextField field) {
        try {
            Double.parseDouble(field.getText());
            return true;
        }catch (Exception e) {
            if (alertLimit < 1) {
                showError("One or more fields are invalid. Please input only integers and/or doubles.");
                alertLimit++;
            }
        }
        return false; //default, otherwise it'd return true
    }

    private boolean areFieldsValid(List<TextField> array) {
        boolean x = false;
        for (TextField tf : array)
            if (isFieldValid(tf))
                x = true;
        return x;
    }

    private List<TextField> getFields() {
        List<TextField> temp = new ArrayList<>(10);
        temp.add(circRad1);
        temp.add(circRad2);
        temp.add(circRad3);
        temp.add(circRad4);
        temp.add(circRad5);
        temp.add(rectLen1);
        temp.add(rectLen2);
        temp.add(rectLen3);
        temp.add(rectLen4);
        temp.add(rectLen5);
        temp.add(rectWid1);
        temp.add(rectWid2);
        temp.add(rectWid3);
        temp.add(rectWid4);
        temp.add(rectWid5);
        return temp;
    }
}
