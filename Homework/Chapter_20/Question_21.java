package Homework.Chapter_20;

import java.util.Comparator;

/**
 * Created on 9/15/2017.
 */
public class Question_21 {

    public static void main(String[] args) {
        GeometricObject[] list = {new Circle(5), new Rectangle(4, 5),
                new Circle(5.5), new Rectangle(2.4, 5), new Circle(0.5),
                new Rectangle(4, 65), new Circle(4.5), new Rectangle(4.4, 1),
                new Circle(6.5), new Rectangle(4, 5)};

        System.out.println("Before sort: " + arrayToString(list));
        selectionSort(list, new GeometricObjectComparator()); //See GeometricObject for notes on why new GeometObjComp()
        System.out.println("After sort: " + arrayToString(list));
    }

    public static <E> void selectionSort(E[] list, Comparator<? super E> comparator) {
    //Make sure to use comparator and not o1.getArea < o2.getArea, etc.
        for (int i = 0; i < list.length; i++) {
            //We need to declare these variables so we can swap after second for loop
            int currIndex = i;
            E currElement = list[currIndex];
            //Check currElement against the next element, tempElement
            for (int j = i + 1; j < list.length; j++) {
                // E tempElement = list[j];
                /*
                 * It will check to see if currElement, which is our min, area is greater than temp's area
                 * So if we want to find the smallest, currElement.getArea has to be GREATER than temp.getArea, therefore == 1
                 */
                if (comparator.compare(currElement, list[j]) == 1) { //If e1's area is greater than e2's area
                    //Found that temp's area is less than currElement
                    //So now change currElement since we found the new min element
                    currElement = list[j];
                    currIndex = j;
                }
            }
            //Now that we've checked currElement against the rest of the list and found the minimum
            //We have to swap the elements

            E temp = list[i]; //cant = currElement b/c we just changed it!
            list[i] = currElement; //change the first element to min
            list[currIndex] = temp; //change the "prev" element to the element we just replaced
        }
    }

    public static String arrayToString(GeometricObject[] array) {
        String temp = "[";
        for (GeometricObject o : array)
            temp += (o + ", ");
        return temp.substring(0, temp.length()-2) + "]";
    }
}
