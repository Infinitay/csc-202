package Homework.Chapter_20;

/**
 * Created on 9/16/2017.
 */
public class Rectangle extends GeometricObject {

    private double length, width;

    public Rectangle() {
        this.length = 2;
        this.width = 2;
    }

    public Rectangle(double x, double y) {
        this.length = y;
        this.width = x;
    }

    @Override
    public double getArea() {
        return (length * width);
    }

    @Override
    public double getPerimeter() {
        return 0;
    }

    @Override
    public String toString() {
        return "Rectangle(" + this.length + ", " + this.width + ")";
    }
}
