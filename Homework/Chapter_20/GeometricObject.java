package Homework.Chapter_20;

import java.util.Comparator;
import java.util.Date;

/**
 * Created on 9/15/2017.
 */
/*
 * Taken from Listing 13.1 in textbook, pointed to it from question to listing 20.4, to listing 13.1
 *
 * Because this class is abstact, it allows for me to extend this class to other classes, such as "Circle"
 * And allow me to utilize the abstract methods here such as getArea to SPECIFICALLY define & get the area of a circle
 * Using (pi)r^2. This is useful because if, and since, I need to make another Rectangle class, I can also define it's appropriate
 * getArea method. Why bother with abstracts? Well, if I were to keep the original lines of code, where it defined color, boolean filled, and dateCreated,
 * I could still access those methods as they weren't abstract methods, which still lets me call such methods for both rect and circ instead of defining them for both classes
 *
 * Why not use an interface instead? Well, interface is kind of like a foundation, you can create the methods that have to be there, but you can't define them
 * Abstracts are kind of like building blocks, I can create some methods, and define them if I want, or leave them abstract to define later.
 *
 * SCRATCH THIS Instead of creating a new class "GeometricObjectComparator", why not just implement it directly here! SCRATCH THIS
 * Question specifices static method to use, and one parameter is comparator, therefore I have to make the comparator within its own seperate class
 * Or I could also create it within the parameters, like a lambda(?) new Comparator<GeometricObject> {......}
 */
public abstract class GeometricObject implements Comparator<GeometricObject> { //Type GeometricObject, otherwise we cant override

    private String color = "White";
    private boolean filled;
    private Date dateCreated;

    /** Construct a default geometric object */
    protected GeometricObject() {
        dateCreated = new Date();
    }

    /** Construct a geometric object with color and filled value */
    protected GeometricObject(String color, boolean filled) {
        dateCreated = new Date();
        this.color = color;
        this.filled = filled;
    }

    /** Return color */
    public String getColor() {
        return color;
    }

    /** Set a new color */
    public void setColor(String color) {
        this.color = color;
    }

    /** Return filled. Since filled is boolean,
     *  the get method is named isFilled */
    public boolean isFilled() {
        return filled;
    }

    /** Set a new filled */
    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    /** Get dateCreated */
    public Date getDateCreated() {
        return dateCreated;
    }

    /** Return a string representation of this object */
    @Override
    public String toString() {
        return "created on " + dateCreated + "\ncolor: " + color +
                " and filled: " + filled;
    }

    /** Abstract method getArea */
    public abstract double getArea();

    /** Abstract method getPerimeter */
    public abstract double getPerimeter();

    @Override
    public int compare(GeometricObject o1, GeometricObject o2) {
        // -1 if < || 0 if = || 1 if >
        double area1 = o1.getArea();
        double area2 = o2.getArea();
        if (area1 < area2)
            return -1; //If Object 1's area is less than area two's, return -1
        else if (area1 == area2)
            return 0; //If Object 1's area is equal to area two's, return 0
        else
            return 1; //If Object 1's area is greater than area two's, return 1
    }
}

