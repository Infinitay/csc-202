package Homework.Chapter_30;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created on 12/14/2017.
 * java.util.ConcurrentModificationException IS SUPPOSED TO HAPPEN
 */
public class Question9 {
    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();

        new Thread(() -> {
            for (int i = 0; i < 1000; i++) {
                System.out.println("Adding " + i + " to hashset.");
                set.add(i);
                try {
                    Thread.sleep(1000); // delay
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(() -> {
            while (true) {
                Iterator iterator = set.iterator();
                while (iterator.hasNext()) {
                    System.out.println("Iterator thread: " + iterator.next());
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
