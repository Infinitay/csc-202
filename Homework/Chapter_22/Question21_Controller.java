package Homework.Chapter_22;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.HashSet;

/**
 * Created on 9/27/2017.
 */
public class Question21_Controller {

    @FXML
    private GridPane mainGrid;

    @FXML
    private Button solveBtn;

    @FXML
    private Button clearBtn;

    private final int ROWS = 9;
    private final int COLS = 9;

    //List<GridPane> gridPaneNodes = new ArrayList<>((ROWS*COLS));
    int[][] tempBox = new int[ROWS][COLS];
    HashSet<TextField> textFieldHashSet = new HashSet<>(81);

    @FXML
    void initialize() {
        Platform.runLater(() -> {
            /*        // Get 3x3 gridpanes
        for (Node node : mainGrid.getChildren())
            if (node instanceof GridPane)
                gridPaneNodes.add((GridPane) node);*/

            // initialize textfield inside gridpane
            //int[] custom = {0, 8, 0, 0, 0, 9, 7, 4, 3, 0, 5, 0, 0, 0, 8, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 8, 0, 4, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 7, 0, 0, 3, 0, 5, 0, 0, 0, 8, 0, 9, 7, 2, 4, 0, 0, 0, 5, 0};
            //int[][] customPuzzle = new SudokuSolver(custom).getGrid();

            for (int x = 0; x < ROWS; x++)
                for (int y = 0; y < COLS; y++) {
                    TextField tf = new TextField();
                    GridPane.setValignment(tf, VPos.CENTER);
                    GridPane.setHalignment(tf, HPos.CENTER);
                    tf.alignmentProperty().setValue(Pos.CENTER);
                    tf.maxHeightProperty().setValue(30);
                    tf.maxWidthProperty().setValue(30);
                    //if (customPuzzle[x][y] != 0)
                     //   tf.setText(String.valueOf(customPuzzle[x][y])); //IF CUSTOM PUZZLE, ELSE COMMENT OUT
                    mainGrid.add(tf, y, x);
                    textFieldHashSet.add(tf);
                }
        });
    }

    @FXML
    private void solve() {
//http://elmo.sbs.arizona.edu/sandiway/sudoku/challenge1.gif
        try {
            boolean readyToSolve = false;
            for (TextField tempField : textFieldHashSet) {
                if (!tempField.getText().isEmpty()) {
                    tempBox[GridPane.getRowIndex(tempField)][GridPane.getColumnIndex(tempField)] = Math.abs(Integer.parseInt(tempField.getText()));
                    readyToSolve = true;
                } else {
                        tempField.setStyle("-fx-text-fill: #ff0000; -fx-font-weight: bold");
                }
            }
            if (readyToSolve) {
                SudokuSolver solver = new SudokuSolver(tempBox);
                solver.solvePuzzle();
                textFieldHashSet.forEach(field -> {
                    field.setText(String.valueOf(tempBox[GridPane.getRowIndex(field)][GridPane.getColumnIndex(field)]));
                });
                showMessage("Solved puzzle! Your solution has been filled in.");
                tempBox = new int[ROWS][COLS];
            }
        } catch (NumberFormatException nfe) {
            showError("Please only input positive real numbers. (1-9)");
        }catch (Exception e) {
            try {
                switch (e.getMessage()) {
                    case "Invalid input": //when !1<=x<=9
                        showError("Please double check input to only contain positive real integers. (1-9)"
                                + "\nAdditionally, please verify your sudoku puzzle is valid. Make sure there are"
                                + " no repeated integers within the same row and/or column, otherwise it's an invalid puzzle.");
                        break;
                    case "No solution":
                        showError("Couldn't find a valid solution for this particular sudoku puzzle."
                                + " Please try again. If you believe there is a error with the program, please"
                                + " first double check your inputs and then try again.");
                        break;
/*                    default:
                        showError(e.toString() + "\n" + e.getMessage() + "\n" + e.getClass()
                                + "\n" + e.getLocalizedMessage() + "\n" + e.getCause());*/
                }
            } catch (Exception ee) {
                showError("Congratulations, here's a cookie.\n\n"
                + "Now please double check input to only contain positive real integers. (1-9)"
                        + "\nAdditionally, please verify your sudoku puzzle is valid. Make sure there are"
                        + " no repeated integers within the same row and/or column, otherwise it's an invalid puzzle.");
                clear();
                e.printStackTrace();
            }
            resetFieldStyle();
        }
    }

    /*private void printArray(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + ", ");
            }
            System.out.println();
        }
    }*/

    @FXML
    private void clear() {
        textFieldHashSet.forEach(field -> {
            field.setText("");
            field.setStyle(null);
        });
        tempBox = new int[ROWS][COLS];
        new SudokuSolver(tempBox).printGrid();
    }

    private void resetFieldStyle() {
        textFieldHashSet.forEach(field -> {
            field.setStyle(null);
        });
        tempBox = new int[ROWS][COLS];
        new SudokuSolver(tempBox).printGrid();
    }
    private void showError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR, message, ButtonType.OK);
        alert.setTitle("Sudoku Solver - Error Detected");
        alert.showAndWait();
    }

    private void showMessage(String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, message, ButtonType.OK);
        alert.setHeaderText("Sudoku Solver - Notification");
        alert.showAndWait();
    }
}
