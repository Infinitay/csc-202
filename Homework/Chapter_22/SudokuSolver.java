package Homework.Chapter_22;

/**
 * Created on 10/1/2017.
 */
public class SudokuSolver {

    private int[][] grid;

    SudokuSolver() {
        this.grid = new int[3][3];
    }

    SudokuSolver(int[] gridArray) {
        this.grid = convertPuzzle(gridArray);
    }
    SudokuSolver(int[][] gridArray) {
        //TODO Exceptions for when gridArray is not 3x3, but then again its handled in controller for 22.21
            this.grid = gridArray;
    }

    public void solvePuzzle() throws Exception {
        if (!isValid(this.grid))
            throw new Exception("Invalid input");
        else if (search(this.grid)) {
            System.out.println("The solution is found:");
            printGrid(this.grid);
        } else
            throw new Exception("No solution");
    }



    /** Obtain a list of free cells from the puzzle */
    public int[][] getFreeCellList(int[][] grid) {
        // Determine the number of free cells
        int numberOfFreeCells = 0;
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (grid[i][j] == 0)
                    numberOfFreeCells++;

        // Store free cell positions into freeCellList
        int[][] freeCellList = new int[numberOfFreeCells][2];
        int count = 0;
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (grid[i][j] == 0) {
                    freeCellList[count][0] = i;
                    freeCellList[count++][1] = j;
                }

        return freeCellList;
    }

    /** Print the values in the grid */
    public void printGrid() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++)
                System.out.print(this.grid[i][j] + " ");
            System.out.println();
        }
    }

    /** Print the values in the grid */
    public void printGrid(int[][] grid) {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++)
                System.out.print(grid[i][j] + " ");
            System.out.println();
        }
    }

    /** Search for a solution */
    public boolean search(int[][] grid) {
        int[][] freeCellList = getFreeCellList(grid); // Free cells
        if (freeCellList.length == 0)
            return true; // "No free cells");

        int k = 0; // Start from the first free cell
        while (true) {
            int i = freeCellList[k][0];
            int j = freeCellList[k][1];
            if (grid[i][j] == 0)
                grid[i][j] = 1; // Fill the free cell with number 1

            if (isValid(i, j, grid)) {
                if (k + 1 == freeCellList.length) { // No more free cells
                    return true; // A solution is found
                }
                else { // Move to the next free cell
                    k++;
                }
            }
            else if (grid[i][j] < 9) {
                // Fill the free cell with the next possible value
                grid[i][j] = grid[i][j] + 1;
            }
            else { // free cell grid[i][j] is 9, backtrack
                while (grid[i][j] == 9) {
                    if (k == 0) {
                        return false; // No possible value
                    }
                    grid[i][j] = 0; // Reset to free cell
                    k--; // Backtrack to the preceding free cell
                    i = freeCellList[k][0];
                    j = freeCellList[k][1];
                }

                // Fill the free cell with the next possible value,
                // search continues from this free cell at k
                grid[i][j] = grid[i][j] + 1;
            }
        }
    }

    /** Check whether grid[i][j] is valid in the grid */
    public boolean isValid(int i, int j, int[][] grid) {
        // Check whether grid[i][j] is valid at the i's row
        for (int column = 0; column < 9; column++)
            if (column != j && grid[i][column] == grid[i][j])
                return false;

        // Check whether grid[i][j] is valid at the j's column
        for (int row = 0; row < 9; row++)
            if (row != i && grid[row][j] == grid[i][j])
                return false;

        // Check whether grid[i][j] is valid in the 3 by 3 box
        for (int row = (i / 3) * 3; row < (i / 3) * 3 + 3; row++)
            for (int col = (j / 3) * 3; col < (j / 3) * 3 + 3; col++)
                if (row != i && col != j && grid[row][col] == grid[i][j])
                    return false;

        return true; // The current value at grid[i][j] is valid
    }

    /** Check whether the fixed cells are valid in the grid */
    public boolean isValid(int[][] grid) {
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++)
                if (grid[i][j] < 0 || grid[i][j] > 9 ||
                        (grid[i][j] != 0 && !isValid(i, j, grid)))
                    return false;

        return true; // The fixed cells are valid
    }

    public int[][] getGrid() {
        return grid;
    }

    public void setGrid(int[][] grid) {
        this.grid = grid;
    }

    public int[][] convertPuzzle(int[] array) {
        int[][] grid = new int[9][9];
        int n = 0;
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++)
                grid[x][y] = array[n++];
        }
        return grid;
    }

    @Override
    public String toString() {
        String answer = "";
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++)
                answer.concat(this.grid[i][j] + " ");
            answer.concat("\n");
        }
        return answer;
    }
}
