package Homework.Chapter_23;

import java.util.Arrays;

/**
 * Created on 10/8/2017.
 */
public class CustomSelectionSort {
    public static void sort(int[] array) {
        int currMinIndex;
        for (int i = 0; i < array.length; i++) {
            currMinIndex = i; //b/c after first pass, we have already sorted first, second, etc etc
            for (int j = i+1; j < array.length; j++) { //start at i++ b/c we dont want to compare arr[1] to array[1] again || cant do i++ moron >.>
                currMinIndex = (array[j] < array[currMinIndex]) ? j : currMinIndex; // if array[j] less than array[currMin], set minIndex to array[j]'s
                /*if (array[j] < array[currMinIndex])
                    currMinIndex = j;*/
            }
        swap(array, i, currMinIndex);
        }
    }

    private static void swap(int[] array, int atIndex, int toIndex) {
        int temp = array[atIndex];
        array[atIndex] = array[toIndex];
        array[toIndex] = temp;
        System.out.println(Arrays.toString(array));
    }

    public static String print(int[] array) {
        return Arrays.toString(array);
    }
}
