package Homework.Chapter_23;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * Created on 10/8/2017.
 */
public class Question15_TEST {

    @Test
    void TestSort() {
        int[] expArray = {5, 1, 10, 8, 3};
        /*for (int i = 0; i < expArray.length; i++)
            expArray[i] = ;//new Random().nextInt(100);*/
        int[] actArray = expArray.clone();
        CustomSelectionSort.sort(expArray);
        Arrays.sort(actArray);
        Assertions.assertEquals(CustomSelectionSort.print(expArray), CustomSelectionSort.print(actArray));
    }
}
