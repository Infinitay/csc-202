package Homework.Chapter_23.GUI;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import java.util.*;

/**
 * Created on 10/8/2017.
 */
public class Question15_Controller {

    @FXML
    private HBox stackHolder;

    @FXML
    private StackPane stackPane1;

    @FXML
    private Rectangle rect1;

    @FXML
    private Label label1;

    @FXML
    private TextField sizeField;

    @FXML
    private Slider speedSlider;

    @FXML
    private Button playBtn;

    @FXML
    private Button pauseBtn;

    @FXML
    private Button stepBtn;

    @FXML
    private Button resetBtn;

    private final double HEIGHT_FACTOR = 2.82; //(280/99)
    private int[] array;
    private SequentialTransition timeline;
    private ObservableList<Node> workingCollection;

    @FXML
    private void initialize() {
        Platform.runLater(() -> {
            stackHolder.managedProperty().setValue(false);
            timeline = new SequentialTransition();
            timeline.setCycleCount(1);
            timeline.rateProperty().bindBidirectional(speedSlider.valueProperty());
            //max rect height = 280
            array = new int[getNumberOfRectangles()];
            sizeField.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) { //when focus lost
                    if(!sizeField.getText().matches("[0-9]{1,2}")){
                        sizeField.setText("20");
                    }
                }
            });
            workingCollection = FXCollections.observableArrayList(stackHolder.getChildren());
            updateRectangles();
        });

    }

    @FXML
    void play() {
        Platform.runLater(() -> {
            System.out.println("Sorting array and creating animation.");
            sort(array);
            System.out.println("Done! Now attempting to play back.");
/*            System.out.println(timeline.getTotalDuration());
            System.out.println(timeline.getRate());*/
            timeline.play();
            timeline.setOnFinished(event -> resetTimeline());
        });
    }

    private int skipTo = 0;
    @FXML
    void step() {
        Platform.runLater(() -> {
            speedSlider.valueProperty().setValue(0);
            timeline.rateProperty().bindBidirectional(speedSlider.valueProperty());
            timeline.play();
            skipTo = skipTo + 20;
            sort(array);
            timeline.jumpTo(Duration.millis(skipTo));
            System.out.println(timeline.getCurrentTime());
        });

    }

    @FXML
    void reset() {
        array = new int[getNumberOfRectangles()];
        updateRectangles();
        resetTimeline();
        skipTo = 0;
    }

    @FXML
    void pause() {
        timeline.pause();
    }

    @FXML
    void displayInfo(MouseEvent event) {
        new Alert(Alert.AlertType.INFORMATION, ((event.getSource()).toString()
                + "\n\n" + ((StackPane)event.getSource()).getLayoutBounds())
                + "\n\n" + ((StackPane)event.getSource()).getBoundsInParent()).show();
    }

    private void resetTimeline() {
        timeline.getChildren().clear();
        timeline = new SequentialTransition();
        timeline.setCycleCount(1);
        timeline.rateProperty().bindBidirectional(speedSlider.valueProperty());
    }

    private ArrayList<StackPane> paneArrayList = new ArrayList<>();

    private void sort(int[] array) {
        int currMinIndex;

        for (int i = 0; i < array.length-1; i++) {
            currMinIndex = i; //b/c after first pass, we have already sorted first, second, etc etc
            animateColorChange(i, Color.rgb(0, 200, 0));
            for (int j = i+1; j < array.length; j++) {
                animateColorChange(j, Color.rgb(200, 0, 0));
                if (array[j] < array[currMinIndex]) {
                    currMinIndex = j;
                }
            }

            animateColorChange(currMinIndex, Color.rgb(0, 0, 200));
            timeline.getChildren().add(new PauseTransition(Duration.millis(250)));

            if (currMinIndex != i) {
                animateColorChange(currMinIndex, Color.rgb(0, 200, 0));
                swap(array, i, currMinIndex);
                for (int k = array.length-1; k > 0; k--) {
                    animateColorChange(k, Color.rgb(255, 255, 255));
                }
            }
            for (int z = 0; z < i+2; z++)
                animateColorChange(z, Color.rgb(0, 200, 0));
        }
        //failsafe for when everythign is sorted, but colors are wrong. ez

    }

    private void swap(int[] array, int atIndex, int toIndex) {
        int temp = array[atIndex];
        array[atIndex] = array[toIndex];
        array[toIndex] = temp;
        swapRect(atIndex, toIndex);
    }

    private void animateColorChange(int rectIndex, Color changeColorTo) {
        Timeline temp = new Timeline();
        temp.setCycleCount(1);
        temp.rateProperty().bindBidirectional(speedSlider.valueProperty());

        Rectangle rect = (Rectangle) ((StackPane)workingCollection.get(rectIndex)).getChildren().get(0);
        timeline.getChildren().add(new Timeline(
                new KeyFrame(Duration.millis(10), new KeyValue(rect.fillProperty(), changeColorTo))));
    }

    private int getNumberOfRectangles() {
        return Integer.parseInt(sizeField.getText());
    }

    private void updateRectangles() {
        Platform.runLater(() -> {
            paneArrayList.clear();
            Set<Integer> randNums = new HashSet<>(20);
            //Set<Integer> randNumbersList = new HashSet<>(array.length);
            if (array.length > 0 && array.length < 21) {
                for (int i = 0; i < array.length; i++) {
                    while (randNums.size() < 20)
                        randNums.add(new Random().ints(10, 100).limit(1).toArray()[0]);

                    Rectangle tempRect = cloneRect(rect1);
                    tempRect.setHeight((Integer)randNums.toArray()[i] * HEIGHT_FACTOR); //Math.PI - Math.sqrt(Math.PI) - 15 maybe?
                    Label tempLabel = cloneLabel(label1);
                    tempLabel.setText(String.valueOf(randNums.toArray()[i]));
                    StackPane tempPane = new StackPane(tempRect, tempLabel);
                    tempPane.setLayoutX(i*30);
                    //tempPane.setLayoutY(280-tempPane.getBoundsInParent().getMinY());
                    array[i] = (Integer)randNums.toArray()[i]; //THIS PIECE OF SHIT, IS ALL IT TOOK TO FIX AN ISSUE I HAD FOR NEARLY 2 HOURSOKWEOPFJWEIOPFJEWIOPFJIOWEFJ
                    tempPane.setId(String.valueOf(i));
                    tempPane.setOnMouseReleased(e -> displayInfo(e));
                    paneArrayList.add(tempPane);
                }
                stackHolder.getChildren().setAll(paneArrayList);
            } /*else
                        showErr*/
            workingCollection = FXCollections.observableArrayList(paneArrayList); //same thread otherwise errors!
        });
    }

    private Rectangle cloneRect(Rectangle cloneThis) {
        Rectangle temp = new Rectangle(cloneThis.getWidth(), cloneThis.getHeight(), cloneThis.getFill());
        temp.arcWidthProperty().setValue(cloneThis.arcWidthProperty().getValue());
        temp.arcHeightProperty().setValue(cloneThis.arcHeightProperty().getValue());
        temp.arcWidthProperty().setValue(cloneThis.arcWidthProperty().getValue());
        temp.smoothProperty().setValue(cloneThis.smoothProperty().getValue());
        temp.strokeProperty().setValue(cloneThis.strokeProperty().getValue());
        temp.strokeTypeProperty().setValue(cloneThis.strokeTypeProperty().getValue());
        temp.strokeWidthProperty().setValue(cloneThis.strokeWidthProperty().getValue());
        temp.strokeDashOffsetProperty().setValue(cloneThis.strokeDashOffsetProperty().getValue());
        temp.strokeLineCapProperty().setValue(cloneThis.strokeLineCapProperty().getValue());
        temp.strokeLineJoinProperty().setValue(cloneThis.strokeLineJoinProperty().getValue());
        temp.strokeMiterLimitProperty().setValue(cloneThis.strokeMiterLimitProperty().getValue());
        temp.styleProperty().setValue(cloneThis.styleProperty().getValue());
        return temp;
    }

    private Label cloneLabel(Label cloneThis) {
        Label temp = new Label(cloneThis.getText());
        temp.setFont(cloneThis.getFont());
        temp.setTextFill(cloneThis.getTextFill());
        temp.alignmentProperty().setValue(cloneThis.alignmentProperty().getValue());
        temp.setStyle(cloneThis.getStyle());
        temp.getCssMetaData().addAll(cloneThis.getCssMetaData());
        temp.applyCss();
        return temp;
    }

    int asf = 1;
    // I hate my life.
    private void swapRect(int atIndex, int toIndex) {
        StackPane rect1 = ((StackPane)workingCollection.get(atIndex));
        StackPane rect2 = ((StackPane)workingCollection.get(toIndex));
/*        double tempX1 = rect1.getBoundsInParent().getMinX();
        double tempX2 = rect2.getBoundsInParent().getMinX();*/
        TranslateTransition t1 = new TranslateTransition();
        t1.setNode(rect1);
        t1.setDuration(Duration.millis(500));
        TranslateTransition t2 = new TranslateTransition();
        t2.setNode(rect2);
        t2.setDuration(Duration.millis(500));

        double displacement = (toIndex - atIndex) * 30;
        t1.setByX(displacement);
        t2.setByX(-displacement);

        ParallelTransition transition = new ParallelTransition();
        transition.getChildren().addAll(t1, t2);
        timeline.getChildren().add(transition);

        /*System.out.println("SWAP # " + asf);
        System.out.println(atIndex+1 + " | " + "From: " + tempX1 + " | To:" + (tempX2));
        System.out.println(toIndex+1 + " | " + "From: " + tempX2 + " | To:" + (tempX1));*/
        Collections.swap(workingCollection, atIndex, toIndex);
        //stackHolder.getChildren().setAll(workingCollection);
        //asf++;
    }
}
