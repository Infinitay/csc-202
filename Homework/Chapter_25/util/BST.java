package Homework.Chapter_25.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by REDACTED on 10/23/2017.
 */
public class BST<E extends Comparable<E>> extends AbstractTree<E> {
    protected TreeNode<E> root; //only BST can access these variables
    protected int size = 0;
    private ArrayList<E> toStringList = new ArrayList<>();

    /**
     * Create a default binary search tree
     */
    public BST() { //empty
    }

    /**
     * Create a binary search tree from an array of objects
     * @param objects
     */
    public BST(E[] objects) {
        for (int i = 1; i < objects.length; i++)
            insert(objects[i]);
    }

    /**
     * Return true if the element is in the tree
     * @param e
     */
    @Override
    public boolean search(E e) {
       TreeNode<E> current = root; //start traversal from current
        while (current != null) {
            if (e.compareTo(current.element) < 0) //when e LESS than current element
                current = current.left;
            else if (e.compareTo(current.element) > 0) //when e GREATER than current element
                current = current.right;
            else //when e EQUALS current element
                return true; //we're done traversing, found element 'e' in our BST
        }
        return false;
    }

    /**
     * Insert element e into the binary search tree.
     * Return true if the element is inserted succesfully
     * @param e
     */
    @Override
    public boolean insert(E e) {
        if (root == null) //if bst empty
            root = createNewNode(e); //create and set root = to new bst consisting of element e
        else {
            //Locate parent node
            TreeNode<E> parent = null;
            TreeNode<E> current = root;

            while(current != null) {
                if (e.compareTo(current.element) < 0) {
                    parent = current;
                    current = current.left;
                } else if (e.compareTo(current.element) > 0) {
                    parent = current;
                    current = current.right;
                } else
                    return false; //no duplicate nodes allowed
            }

                //Found parent node...
                //Create a new node and attach it to the parent
                if (e.compareTo(parent.element) < 0)
                    parent.left = createNewNode(e);
                else
                    parent.right = createNewNode(e);
        }
        //we created a new node
        size++;
        return true;
    }

    protected TreeNode<E> createNewNode(E e) {
        return new TreeNode<>(e); //create and return a new treenode
    }

    /**
     * Delete the specified element from the tree.
     * Return true if the element is deleted successfully.
     * Return false if the element is not in the tree.
     * @param e
     */
    @Override
    public boolean delete(E e) {
        //Locate the node to be deleted, and its parent
        TreeNode<E> parent = null;
        TreeNode<E> current = root;

        while (current != null) {
            if (e.compareTo(current.element) < 0) {
                parent = current;
                current = current.left;
            } else if (e.compareTo(current.element) > 0) {
                parent = current;
                current = current.right;
            } else // we've reached the node we need and set parent and current respectively
                break; // Element is in the tree pointed at by current
        }

        if (current == null) //doesnt go thru while loop b/c !(current != null)
            return false; // element not found

        //Case 1: current has no left child, but we have a right
        if (current.left == null) {
            //Connect the parent with the right child of the current node
            if (parent == null)
                root = current.right;
            else {
                if (e.compareTo(parent.element) < 0)
                    parent.left = current.right;
                else
                    parent.right = current.right;
            }
        } else {
            //Case 2: The current node has a left child
            //In that case, locate the rightmost node in the left subtree of the current
            //node and also its parent
            TreeNode<E> parentOfRightMost = current;
            TreeNode<E> rightMost = current.left;

            while(rightMost.right != null) {
                parentOfRightMost = rightMost;
                rightMost = rightMost.right; //keep going left
            }
            //Replace the element in current by the element in rightMost
            current.element = rightMost.element;

            //Now delete rightmost
            if (parentOfRightMost.right == rightMost)
                parentOfRightMost.right = rightMost.left;
            else //SPECIAL CASE: parentOfRightMost == curent
                parentOfRightMost.left = rightMost.left;
        }
        size--;
        return true; //we've deleted the element
    }

    /**
     * Inorder traversal from the root
     */
    @Override
    public void inorder() {
        inorder(root);
    }

    /**
     * Inorder traversal from a subtree
     * @param root
     */
    protected void inorder(TreeNode<E> root) {
        if (root == null)
            return;
        inorder(root.left);
        //System.out.print(root.element + " ");
        toStringList.add(root.element);
        inorder(root.right);
    }

    /**
     * Postorder traversal from the root
     */
    @Override
    public void postorder() {
        postorder(root);
    }

    /**
     * Postorder traversal from a subtree
     */
    protected void postorder(TreeNode<E> root) {
        if (root == null)
            return;
        postorder(root.left);
        postorder(root.right);
        //System.out.print(root.element + " ");
        toStringList.add(root.element);
    }

    /**
     * Preorder traversal from the root
     */
    @Override
    public void preorder() {
        preorder(root);
    }

    /**
     * Preorder traversal from a subtree
     */
    protected void preorder(TreeNode<E> root) {
        if (root == null)
            return;
        //System.out.print(root.element + " ");
        toStringList.add(root.element);
        preorder(root.left);
        preorder(root.right);
    }

    /**
     * Get the number of nodes in the tree
     */
    @Override
    public int getSize() {
        return size;
    }

    /**
     * Returns the root of the tree
     * @return root
     */
    public TreeNode<E> getRoot() {
        return root;
    }

    /**
     * Returns a path from the root leading to the specified element
     * @param e
     * @return list - path from root to get to e
     */
    public ArrayList<TreeNode<E>> path(E e) {
        ArrayList<TreeNode<E>> list = new ArrayList<>();
        TreeNode<E> current = root; //initialize current to root

        while (current != null) {
            list.add(current); //add current to list

            if (e.compareTo(current.element) < 0)
                current = current.left;
            else if (e.compareTo(current.element) > 0)
                current = current.right;
            else
                break; //exit out of while loop b/c we've reached the element we're searching for
        }
        return list;
    }

    /**
     * Remove all elements from the tree
     */
    public void clear() {
        root = null;
        size = 0;
    }

    public String traversalToString() {
        ArrayList<E> temp = toStringList;
        toStringList = new ArrayList<>();
        return temp.toString();
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     * @return an Iterator.
     */
    @Override
    public Iterator<E> iterator() {
        return new InorderIterator();
    }

    private class InorderIterator implements Iterator<E> {
        //Stores the elements into a list
        private ArrayList<E> list = new ArrayList<>();
        private int current = 0; //Point to the current element in the list

        public InorderIterator() {
            inorder(); //traverse the binary tree and store elements in list
        }

        /**
         * Inorder traversal from the root
         */
        private void inorder() {
            inorder(root);
        }

        /**
         * Inorder traversal from a subtree
         */
        private void inorder(TreeNode<E> root) { // "overrides" super inorder method
            if (root == null)
                return;
            inorder(root.left);
            list.add(root.element);
            inorder(root.right);
        }

        /**
         * More elements for traversing?
         */
        @Override
        public boolean hasNext() {
            if (current < list.size())
                return true;
            else
                return false;
        }

        /**
         * Get the current element and move to the next
         */
        @Override
        public E next() {
            return list.get(current++);
        }

        /**
         * Remove the current element
         */
        @Override
        public void remove() {
            delete(list.get(current));
            list.clear();
            inorder();
        }
    }

    /**
     * This inner class is static, because it does not access any instance members defined in its outer class
     */

    public static class TreeNode<E extends Comparable<E>> {
        protected E element; //only treenode can access these variables
        protected TreeNode<E> left;
        protected TreeNode<E> right;

        public TreeNode(E e) {
            element = e;
        }
    }
}
