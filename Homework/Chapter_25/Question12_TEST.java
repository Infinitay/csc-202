package Homework.Chapter_25;

import Homework.Chapter_25.util.BST;

/**
 * Created by REDACTED on 10/23/2017.
 */
public class Question12_TEST {

    public static void main(String[] args) {
        //Integer[] numbs  = {23, 34, 1, 43, 5, 0, 42, 2, 5, 12, 5, 05, 02, 100, 25, 95, 78};
        BST<String> myBST = new BST<>();
        myBST.insert("John");
        myBST.insert("Michael");
        myBST.insert("Jordan");
        myBST.insert("Alex");

        //Traversal Methods
        String[] traversalNames = {"Inorder", "Postorder", "Preorder"};
        for (int i = 0; i < 3; i++) {
            System.out.println("/============= " + traversalNames[i] + " =============\\");
            chooseTraversalMethod(myBST, i);
            System.out.println("\\============= " + traversalNames[i] + " =============/");
        }
        System.out.println("");

        //Query/Accessor Methods
        System.out.println("Is our BST empty?");
        if (!myBST.isEmpty()) {
            System.out.println("No! It's size is " + myBST.getSize());
            //System.out.println("The root of our BST is " + myBST.getRoot());
            System.out.println("Searching for John..." + myBST.search("John"));
            System.out.println("Searching for Dad..." + myBST.search("Dad")); //

        } else
            System.out.println("Yes, unfortunately, now my BST and bank account are similar.");

        //Update/Mutator Methods
        if (!myBST.isEmpty()) {
            System.out.println("Let's do something about getting us a Dad, shall we?");
            System.out.println("Have we added a Dad? " + myBST.insert("Dad"));
            System.out.println("Now lets see our new list in order!");
            myBST.inorder();
            System.out.println(myBST.traversalToString());
            System.out.println("Okay lets remove Jordan because Kobe is better..." + myBST.delete("Jordan"));
            myBST.inorder();
            System.out.println(myBST.traversalToString());
            System.out.println("Now now, don't get triggered, here, I'll just clear the list.");
            myBST.clear();
            if(myBST.isEmpty()) {
                System.out.println("There happy? Now our BST is empty.");
                myBST.inorder();
                System.out.println(myBST.traversalToString());
            }
        }
    }

    private static void chooseTraversalMethod(BST x, int y) {
        if (y == 0) {
            x.inorder();
        }else if (y == 1)
            x.postorder();
        else if (y == 2)
            x.preorder();
        else
            System.exit(1);

        System.out.println(x.traversalToString());
    }
}
