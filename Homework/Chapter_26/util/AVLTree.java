package Homework.Chapter_26.util;

import Homework.Chapter_25.util.BST;

/**
 * Created by REDACTED on 11/6/2017.
 */
public class AVLTree<E extends Comparable<E>> extends BST<E> {

    /**
     * Create an empty AVL Tree
     */
    public AVLTree() {}

    /**
     * Create an AVL Tree from an array of objects
     */

    public AVLTree(E[] objects) {
        super(objects);
    }

    /**
     * Override createNewNode to create an AVLTreeNode
     * @param e
     * @return
     */
    @Override
    protected AVLTreeNode<E> createNewNode(E e) {
        return new AVLTreeNode<E>(e);
    }

    /**
     * An AVL Treenode is a BST TreeNode, but with a height
     * @param <E>
     */
    protected static class AVLTreeNode<E extends Comparable<E>> extends BST.TreeNode<E> {
        protected int height = 0; //new data field

        public AVLTreeNode(E e) {
            super(e);
        }
    }
}
