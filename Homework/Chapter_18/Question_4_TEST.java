package Homework.Chapter_18;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Created on 8/27/2017.
 */
class Question_4_TEST {

    
    @Test
    void sum() {
        for (int i = 1; i <= 10; i++) {
            //                          expected,   actual, delta = "percent error" kinda
            // b/c for example when i = 4, without a delta specificed, test would fail b/c 2.45 1= 2.4499999999999997
            Assertions.assertEquals(expectedSum(i), Question_4.sum(i), 0.01, "i = " + i);
        }
    }

    @Test
    void sumBig() {
        for (int i = 1; i <= 10; i++) {
            Assertions.assertEquals(expectedSum(i), Question_4.sumBig(i).doubleValue(), 0.01, "i = " + i);
        }
    }

    private double expectedSum(int i) {
        double answer = 0;
            for (int x = i; x != 0 && i > 0; x--) {
                answer += Math.pow(x, -1);
    }
        return answer;
    }
}