package Homework.Chapter_18;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * Created by REDACTED on 8/23/2017.
 */

public class Question_4 {

    static final MathContext MC = new MathContext(5, RoundingMode.HALF_UP);

    public static void main(String[] args) {
        try {
            for (int i = 1; i <= 10; i++) {
                System.out.println("(1 / " + i + "): " + (writeOut(i)) + "| Sum: " + sum(i)); // USING DOUBLE
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Exercise only states to print from i = 1 to i = 10, nice try.");
            System.out.println("Please only enter positive real numbers next time.");
        }
    }

    public static String writeOut(int i) {
        if (i < 0)
            throw new IllegalArgumentException();
        if (i != 0)
            return "(1 / " + i + ") + "+writeOut(i-1);
        else // base case
            return "";
    }

    public static double sum(int i) {
        if (i < 0)
            throw new IllegalArgumentException();
        if (i == 1) // base case
            return (double) 1;
        else
            return  (double) 1 / i + sum(i - 1);
    }

    public static BigDecimal sum = new BigDecimal(0);

    public static BigDecimal sumBig(int i) {
        if (i < 0)
            throw new IllegalArgumentException();
        if (i == 1) // base case
            return new BigDecimal(1.0);
        else
            return sum = sumBig(i-1).add(new BigDecimal(1).divide(new BigDecimal(i), MC));
    }

    public static double actualSum(double i) { // for testing
        if (i != 0)
            return Math.pow(i, -1) + actualSum(i - 1);
        else // base case
            return 0;
    }
}
