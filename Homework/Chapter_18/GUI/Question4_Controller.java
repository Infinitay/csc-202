package Homework.Chapter_18.GUI;

import Homework.Chapter_18.Question_4;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Question4_Controller {

    @FXML
    private Label writeFractionsLabel;

    @FXML
    private Button answerButton;

    @FXML
    private TextField powerOf;

    @FXML
    void compute(ActionEvent event) {
        try { //https://stackoverflow.com/questions/15615890/recommended-way-to-restrict-input-in-javafx-textfield
            int n = Integer.parseInt(powerOf.getText());
            if (n < 1000)
                writeFractionsLabel.setText(String.valueOf(getSumDouble(n)));
            else
                writeFractionsLabel.setText(String.valueOf(getSumBig(n)));
        } catch(Exception e) { //java.lang.NumberFormatException: For input string: "d"
            new Alert(Alert.AlertType.ERROR, e.toString() + "\n Please make sure to input a positive real number."
            + "\n Exercise states i = 10.").show();
            powerOf.setText("10");
        }
    }

    /*
     * The reason for the if statement, and for sumBig is because I decided to give the user freedom to define 'i'.
     * If the user were too specify an 'i' value greater than 1,000, stackoverflow would occur.
     * In retrospect, it's redundant seeing as exercise only stated to get hte summation of 1/i, from i = 1 to i = 10
     * So instead I just made it so in the gui, the textbox containing i defaults to 10.
     */
    private double getSumDouble(int x) {
        double answer = Question_4.sum(x);
        System.out.println("Input: " + x + " | Result: " + answer);
        return answer;
    }

    private double getSumBig(int x) {
        double answer = sumBig(x).doubleValue();
        System.out.println("Input: " + x + " | Result: " + answer);
        return answer;
    }

    final MathContext MC = new MathContext(5, RoundingMode.HALF_UP);
    public BigDecimal sum = new BigDecimal(0);

    public BigDecimal sumBig(int i) {
        if (i < 0)
            throw new IllegalArgumentException();
        if (i == 1) // base case
            return new BigDecimal(1);
        else
            return sum = sumBig(i-1).add(new BigDecimal(1).divide(new BigDecimal(i), MC));
    }
}
