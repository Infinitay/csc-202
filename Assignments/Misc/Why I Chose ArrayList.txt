First and foremost, if I'm being honest, I chose ArrayList because it was easy to implement.
Originally I planned on implementing a HashMap because I think it'd be the most beneficial for this project as the complexity for accessing users
and their data would be very good, I believe the HashMap's big-O is O(1) because we are getting the value of a specific key without having to traverse
through every single element, especially if implemented using LinkedList IIRC.

Anyways, the worst case of an ArrayList is O(n), which isn't too bad in regards to our project and uses. It's a basic dynamic data structure
allowing us to add, remove, find, and create a new list with relative ease and efficiency, especially over a simple array.