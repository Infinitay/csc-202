package Testing;

import javafx.application.Application;
import javafx.stage.Stage;

import java.io.*;

/**
 * Created on 9/3/2017.
 */
public class FileReadnWriteTesting extends Application {

    public static void main(String[] args) {

        /*User me = new User();
        User he = new User();
        me.setUsername("mex");
        he.setUsername("LUL");
        ArrayList<User> users = new ArrayList<User>();
        try {
            writeUsers(me);
            writeUsers(he);
            users.add((User) readUsers());
            if (users.isEmpty())
                System.out.println(true);
        } catch (Exception e) {
            if (e instanceof FileNotFoundException) // https://stackoverflow.com/a/7213150/7835042
                try {
                    System.out.println("Creating empty file...");
                    writeUsers(me);
                    Platform.runLater( // https://stackoverflow.com/a/17851019/7835042 || Lambda form
                            () -> new Alert(Alert.AlertType.ERROR, "File not found. Created a new file, please re-run.").show()
                    );
                    //new ObjectOutputStream(new FileOutputStream("users.dat")).write(null);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            else if (e instanceof NullPointerException)
                try {
                    System.out.println("No users found. Creating null user.");
                    writeUsers(null);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            else
                e.printStackTrace();
        }
        try {
            writeUsers(he);
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (User u : users)
            System.out.println(u);*/
    }

    // prof tanes tutorial

    public static void writeUsers(Object data) throws IOException {
        ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream("users.dat", true));
        output.writeObject(data);
    }

    public static Object readUsers() throws IOException, ClassNotFoundException {
        ObjectInputStream input = new ObjectInputStream(new FileInputStream("users.dat"));
        return input.readObject();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        launch();
    }
}
